/**
 * @file
 * Jquery Update  Related  Message.
 */

/* ---------------------------------------------
 * Filename:     non.js
 * Website:      http://www.drupal1000.com
 * Description:  Jquery version verifying script.
 * Author:       Mr.Irfan Khan
-----------------------------------------------*/
jQuery(document).ready(function () {
  "use strict";
  jQuery("#header").append('<div class="messages error">Bootstrap 3 requires jQuery 1.9.1 or greater version, Please install the <a href="https://www.drupal.org/project/jquery_update">jQuery Update module</a>  and Select the greater version from Appearance >> Future look theme settings</div>');
});
