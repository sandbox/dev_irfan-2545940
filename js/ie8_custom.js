/**
 * @file
 * IE8 modification java script.
 */

 /* ---------------------------------------------
 * Filename:     non.js
 * Website:      http://www.drupal1000.com
 * Description:  IE8 (Internet Explorer 8 ) Fixes
 * Author:       Mr.Irfan Khan
-----------------------------------------------*/
jQuery(document).ready(function () {
  "use strict";
  jQuery("body").css({marginTop: "0px"});
  jQuery("nav").css({position: "relative"});
  if (jQuery("body").height() <= jQuery(window).height()) {
    jQuery(".footer-background").css({bottom: "0px", left: "0px", position: "absolute", width: "100%"});
  }
});
