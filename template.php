<?php
/**
 * @file
 * Theme setting callbacks for the future look theme.
 */

/**
 * Override theme_textfield.
 */
function future_look_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element,
  array('id', 'name', 'value', 'size', 'maxlength'));
  _form_set_class($element, array('form-text', 'form-control'));
  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';
    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value']
      = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $attributes['class'][] = 'form-control';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }
  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
  return $output . $extra;
}
/**
 * Override theme_password.
 */
function future_look_password($variables) {
  // Password field class change.
  $element = $variables['element'];
  $element['#attributes']['type'] = 'password';
  element_set_attributes($element,
    array('id', 'name', 'value', 'size', 'maxlength'));
  _form_set_class($element, array('form-text', 'form-control'));
  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
  return $output;
}
/**
 * Override theme_button.
 */
function future_look_button($variables) {
  // Button field class change .
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));
  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  $element['#attributes']['class'][] = 'btn';
  $element['#attributes']['class'][] = 'btn-custom';
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }
  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}
/**
 * Override theme_image.
 */
function future_look_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);
  $attributes['class'] = 'img-responsive';

  foreach (array('width', 'height', 'alt', 'title') as $key) {

    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }

  return '<img' . drupal_attributes($attributes) . ' />';
}
/**
 * Implements hook_js_alter().
 */
function future_look_js_alter(&$javascript) {
  $bootstrap_js_path = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js';
  $javascript[$bootstrap_js_path]
    = array(
      'data' => $bootstrap_js_path,
      'scope' => 'header',
      'group' => 'JS_LIBRARY',
      'every_page' => TRUE,
      'type' => 'external',
      'cache' => TRUE,
      'weight' => 0,
      'preprocess' => 1,
      'defer' => 0,
    );
  $keys = array_keys($javascript);
  foreach ($keys as $key) {
    if (strpos($key, "/jquery.js") !== FALSE || strpos($key, "/jquery.min.js") !== FALSE) {
      if (version_compare($javascript[$key]["version"], "1.9.1") == -1) {
        $path_non_js = drupal_get_path('theme', 'future_look') . '/js/non.js';
        $javascript[$path_non_js] = array(
          'data' => $path_non_js,
          'scope' => 'header',
          'group' => 'JS_LIBRARY',
          'every_page' => TRUE,
          'type' => 'file',
          'cache' => TRUE,
          'weight' => 0,
          'preprocess' => 1,
          'defer' => 0,
        );
      }
    }
  }
}
/**
 * Override theme_webform_email.
 */
function future_look_webform_email($variables) {
  $element = $variables['element'];
  if (!isset($element['#attributes']['type'])) {
    $element['#attributes']['type'] = 'email';
  }
  $element['#attributes']['autocomplete'] = 'on';
  // Convert properties to attributes on the element if set.
  foreach (array('id', 'name', 'value', 'size') as $property) {
    if (isset($element['#' . $property]) && $element['#' . $property] !== '') {
      $element['#attributes'][$property] = $element['#' . $property];
    }
  }
  _form_set_class($element, array('form-control', 'form-text', 'form-email'));
  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}
/**
 * Override theme_webform_number.
 */
function future_look_webform_number($variables) {
  $element = $variables['element'];
  if (!isset($element['#attributes']['type'])) {
    $element['#attributes']['type'] = 'number';
  }
  // Convert properties to attributes on the element if set.
  foreach (array('id', 'name', 'value', 'size') as $property) {
    if (isset($element['#' . $property]) && $element['#' . $property] !== '') {
      $element['#attributes'][$property] = $element['#' . $property];
    }
  }
  _form_set_class($element, array('form-control'));
  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}
/**
 * Override theme_table.
 */
function future_look_table($variables) {
  $header = $variables['header'];
  $rows = $variables['rows'];
  $attributes = $variables['attributes'];
  $caption = $variables['caption'];
  $colgroups = $variables['colgroups'];
  $sticky = $variables['sticky'];
  $empty = $variables['empty'];
  // Add sticky headers, if applicable.
  if (count($header) && $sticky) {
    // Add 'sticky-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
  }
  $attributes['class'][] = 'table';
  $output = '<table' . drupal_attributes($attributes) . ">\n";
  if (isset($caption)) {
    $output .= '
    <caption>' . $caption . "</caption>
    \n";
  }
  // Format the table columns.
  if (count($colgroups)) {
    foreach ($colgroups as $number => $colgroup) {
      $attributes = array();
      // Check if we're dealing with a simple or complex column.
      if (isset($colgroup['data'])) {
        foreach ($colgroup as $key => $value) {
          if ($key == 'data') {
            $cols = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cols = $colgroup;
      }
      // Build colgroup.
      if (is_array($cols) && count($cols)) {
        $output .= ' <colgroup' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cols as $col) {
          $output .= ' <col' . drupal_attributes($col) . ' />';
        }
        $output .= " </colgroup>\n";
      }
      else {
        $output .= ' <colgroup' . drupal_attributes($attributes) . " />\n";
      }
      if ($number === 99999) {
        // Do nothing.
      }
    }
  }
  // Add the 'empty' row message if available.
  if (!count($rows) && $empty) {
    $header_count = 0;
    foreach ($header as $header_cell) {
      if (is_array($header_cell)) {
        $header_count += isset($header_cell['colspan']) ? $header_cell['colspan'] : 1;
      }
      else {
        $header_count++;
      }
    }
    $rows[] = array(
      array(
        'data' => $empty,
        'colspan' => $header_count,
        'class' => array('empty', 'message'),
      ),
    );
  }
  // Format the table header.
  if (count($header)) {
    $ts = tablesort_init($header);
    // HTML requires that the thead tag has tr tags in it followed by tbody.
    // Tags. Using ternary operator to check and see if we have any rows.
    $output .= (count($rows) ? '
    <thead>
    <tr>' : '
    <tr>');
    foreach ($header as $cell) {
      $cell = tablesort_header($cell, $header, $ts);
      $output .= _theme_table_cell($cell, TRUE);
    }
    // Using ternary operator to close the tags based on whether or;
    // Not there are rows.
    $output .= (count($rows) ? "
    </tr>
    </thead>
    \n" : "</tr>\n");
  }
  else {
    $ts = array();
  }
  // Format the table rows.
  if (count($rows)) {
    $output .= "
    <tbody>\n";
    $flip = array('even' => 'odd', 'odd' => 'even');
    $class = 'even';
    foreach ($rows as $number => $row) {
      // Check if we're dealing with a simple or complex row.
      if (isset($row['data'])) {
        $cells = $row['data'];
        $no_striping = isset($row['no_striping']) ? $row['no_striping'] : FALSE;
        // Set the attributes array and exclude 'data' and 'no_striping'.
        $attributes = $row;
        unset($attributes['data']);
        unset($attributes['no_striping']);
      }
      else {
        $cells = $row;
        $attributes = array();
        $no_striping = FALSE;
      }
      if (count($cells)) {
        // Add odd/even class.
        if (!$no_striping) {
          $class = $flip[$class];
          $attributes['class'][] = $class;
        }
        // Build row.
        $output .= ' <tr' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cells as $cell) {
          $cell = tablesort_cell($cell, $header, $ts, $i++);
          $output .= _theme_table_cell($cell);
        }
        $output .= " </tr>\n";
      }
    }
    $output .= "
    </tbody>
    \n";
  }
  $output .= "</table>\n";
  return "
  <div class='table-responsive'>" . $output . "</div>";
}
/**
 * Override theme_preprocess_views_view_table.
 */
function future_look_preprocess_views_view_table(&$vars) {
  // Add tablesorter class in view <table>.
  $vars['classes_array'][] = 'table';
}
/**
 * Override theme_textarea.
 */
function future_look_textarea($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('form-control', 'form-textarea'));
  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );
  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }
  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return $output;
}
/**
 * Override theme_select.
 */
function future_look_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-control', 'form-select'));
  return '<select' . drupal_attributes($element['#attributes']) . '>' .
  form_select_options($element) . '</select>';
}
/**
 * Primary and secondry menu prefix and sufix add class.
 */
function future_look_menu_local_tasks(&$variables) {
  $output = '';
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '
    <h2 class="element-invisible">' . t('Primary tabs') . '</h2>
    ';
    $variables['primary']['#prefix'] .= '
    <ul class="nav-tabs  nav ">';
    $variables['primary']['#suffix'] = '
    </ul>
    ';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '
    <h2 class="element-invisible">' . t('Secondary tabs') . '</h2>
    ';
    $variables['secondary']['#prefix'] .= '
    <ul class="nav-tabs nav ">';
    $variables['secondary']['#suffix'] = '
    </ul>
    ';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}
/**
 * Primary and secondry menu nav nav-bar menubar.
 */
function future_look_preprocess_page(&$vars) {
  $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
  $vars['main_menu'] = $main_menu_tree;
  // Get the entire main menu tree.
  $main_menu_tree = menu_tree_all_data('main-menu');
  // Add the rendered output to the $main_menu_expanded variable.
  $vars['main_menu_expanded'] = menu_tree_output($main_menu_tree);
  // Menu create edit.
  if (isset($vars['main_menu'])) {
    $vars['primary_nav'] = theme('links__system_main_menu', array(
    'links' => $vars['main_menu'],
    'attributes' => array(
      'class' => array('nav', 'navbar-nav', 'links', 'inline', 'main-menu'),
    ),
    'heading' =>
    array(
      'text' => t('Main menu'),
      'level' => 'h2',
      'class' => array('element-invisible'),
    ),
    )
    );
  }
  else {
    $vars['primary_nav'] = FALSE;
  }
}
/**
 * Implements Future_look_links__system_main_menu.
 */
function future_look_links__system_main_menu($variables) {
  $menu_name = variable_get('menu_main_links_source', 'main-menu');

  $menu_tree = menu_tree($menu_name);
  return drupal_render($menu_tree);
}
/**
 * Implements Future_look_menu_tree__main_menu.
 */
function future_look_menu_tree__main_menu($variables) {
  return '
  <ul class="nav navbar-nav  inline">' . $variables['tree'] . '</ul>
  ';
}
/**
 * Override theme_menu_link.
 */
function future_look_menu_link(array $variables) {
  // Bootstrap Drop Down Primary Menu link.
  $element = $variables['element'];
  $sub_menu = '';
  if ($element['#below']) {
    // Prevent dropdown functions from being added to management menu so it.
    // Does not affect the navbar module.
    if (($element['#original_link']['menu_name'] == 'main_menu')) {
      $sub_menu = drupal_render($element['#below']);
    }
    elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] >= 1)) {
      // Add our own wrapper.
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '
      <ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>
      ';
      // Generate as standard dropdown.
      if ($element['#original_link']['depth'] > 1) {
        $element['#attributes']['class'][] = 'dropdown-submenu';
      }
      else {
        $element['#attributes']['class'][] = 'dropdown';
        $element['#title'] .= ' <span class="caret"></span>';
      }
      $element['#localized_options']['html'] = TRUE;
      // Set dropdown trigger element to # to prevent inadvertent page loading.
      // When a submenu link is clicked.
      $element['#localized_options']['attributes']['data-target'] = '#';
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
    }
  }
  $element['#localized_options']['attributes']['class'][] = 'menu-text-color';
  $element['#localized_options']['attributes']['class'][] = 'agri-nav-item';
  $element['#localized_options']['attributes']['class'][] = 'nav-space';
  // Set the parent active when child is currently selected.
  if (in_array("active-trail", $element['#attributes']['class'])) {
    $element['#attributes']['class'][] = 'active';
    $element['#attributes']['class'][] = 'open';
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '
    <front>
  ' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li ' . drupal_attributes($element['#attributes']) . ' style="display:block;">' . $output . $sub_menu . "</li>\n";
}
/**
 * Override theme_preprocess_html.
 */
function future_look_preprocess_html(&$variables) {
  // Html.tpl.php file varialble for IE8 css file and js add.
  $bootstrap_min = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css";
  drupal_add_css($bootstrap_min,
     array('group' => CSS_DEFAULT, 'type' => 'external', 'weight' => -115));
  $font_css_url = theme_get_setting('font_css_url');
  if ($font_css_url != "") {
    drupal_add_css($font_css_url);
  }
  if (file_exists("public://future_look.css") === TRUE) {
    drupal_add_css("public://future_look.css", array('group' => CSS_THEME, 'type' => 'file'));
  }
  else {
    drupal_add_css(drupal_get_path("theme", "future_look") . '/css/default.future_look.css',
        array('group' => CSS_THEME, 'type' => 'file')
    );
  }
}
