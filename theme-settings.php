<?php
/**
 * @file
 * Theme setting callbacks for the future look theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function future_look_form_system_theme_settings_alter(&$form, &$form_state) {
  drupal_add_js(drupal_get_path("theme", "future_look") . "/jscolor/jscolor.js", array('preprocess' => FALSE));
  $form['theme_settings']['future_look_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Breadcrumb'),
    '#default_value' => theme_get_setting('future_look_breadcrumb'),
    '#tree' => FALSE,
  );
  $form['future_look_settings_color'] = array(
    '#type' => 'fieldset',
    '#title' => t('Body Background-image & Color Settings'),
    '#description' => t("Set Background Image and Background color of page body"),
  );
  $form['future_look_settings_fonts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Font Settings'),
    '#description'   => t("Set Font Setting Through URL </br>For Ex. Font CSS Url :-'https://fonts.googleapis.com/css?family=Open+Sans' ,</br>
    Font-family: 'Open Sans', 'sans-serif'  ,</br>
    Default Font Size :15 px "),
  );
  $form['future_look_settings_menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu  Color Settings'),
    '#description'   => t("Set Main Menu-bar Color Setting  Menu-bar Text Color ,Menu-bar Background Color (gradient color top&bottom) ,Hover color."),
  );
  $form['future_look_settings_slider'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front Page Slider Settings'),
    '#description'   => t("If You want front page slider on your site please enable this check box and select your images and write text in boxes that appear on your front page slides."),
  );
  $form['future_look_settings_tabs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tabs Color Settings'),
    '#description'   => t("Tab color settings."),
  );
  $form['future_look_settings_social'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Icon Link & Icon-Color Settings'),
    '#description'   => t("Insert here your social page link in perticular text field .</br>for example : facebook:'https://www.facebook.com/Drupal-8427738891/'. </br> also select the color of social icon's ."),
  );
  $form['future_look_settings_table'] = array(
    '#type' => 'fieldset',
    '#title' => t('Table (Head,Row,Border,Text) Color Settings'),
    '#description'   => t("Set here all Table related color settings . </br>Table Head color , Table even & odd row color , Table text color , Table border color ."),
  );
  $form['future_look_settings_button'] = array(
    '#type' => 'fieldset',
    '#title' => t('Button Color Settings'),
    '#description'   => t("Set the Button color setting . Button background color (gradient color(top & bottom )),Button Text color ,Button Hover color."),
  );
  $form['future_look_settings_intro'] = array(
    '#type' => 'fieldset',
    '#title' => t('Introduction Region Background-image & Color Settings'),
    '#description'   => t("Set Introduction Region Backgroung-Color Or Background-Image ,Text-Color ."),
  );
  $form['future_look_settings_right_bar_color'] = array(
    '#type' => 'fieldset',
    '#title' => t('RightBar Region Color Settings'),
    '#description'   => t("Set RightBar  Region Backgroung-Color ,Text-Color,Border-Color,Link-Color Settings ."),
  );
  $form['future_look_settings_box1_setting'] = array(
    '#type' => 'fieldset',
    '#title' => t('First-Box Region Color Settings'),
    '#description'   => t("Set First-Box Region Backgroung-Color ,Text-Color,Border-Color."),
  );
  $form['future_look_settings_box2_setting'] = array(
    '#type' => 'fieldset',
    '#title' => t('Secound-Box Region Color Settings'),
    '#description'   => t("Set Secound-Box Region  Backgroung-Color ,Text-Color,Border-Color."),
  );
  $form['future_look_settings_box3_setting'] = array(
    '#type' => 'fieldset',
    '#title' => t('Third-Box Region Color Settings'),
    '#description'   => t("Set Third-Box Region  Backgroung-Color ,Text-Color,Border-Color."),
  );
  $form['future_look_settings_content_bottom_color'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Bottom Region color settings'),
    '#description'   => t("Set Content Bottom Region  Backgroung-Color ,Text-Color,Border-Color."),
  );
  $form['future_look_settings_color_footer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Footer color settings'),
  );
  // Table.
  $form['future_look_settings_table']['thead_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Table Header Text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('thead_color') != "") ? theme_get_setting('thead_color') : "FFFFFF",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['thead_bg'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Table Header Background'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('thead_bg') != "") ? theme_get_setting('thead_bg') : "000000",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['thead_link'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Table Header Link Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('thead_link') != "") ? theme_get_setting('thead_link') : "e3e3e3",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['odd_row_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Odd Row Text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('odd_row_color') != "") ? theme_get_setting('odd_row_color') : "FFFFFF",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['odd_row_bg'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Odd Row Background'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('odd_row_bg') != "") ? theme_get_setting('odd_row_bg') : "474747",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['odd_row_link'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Odd Row Link Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('odd_row_link') != "") ? theme_get_setting('odd_row_link') : "e8e8e8",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['even_row_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Even Row Text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('even_row_color') != "") ? theme_get_setting('even_row_color') : "fff2f2",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['even_row_bg'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Even Row Background'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('even_row_bg') != "") ? theme_get_setting('even_row_bg') : "545454",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['even_row_link'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Even Row Link Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('even_row_link') != "") ? theme_get_setting('even_row_link') : "FFFFFF",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_table']['table_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Table Border Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('table_border_color') != "") ? theme_get_setting('table_border_color') : "e8e5e5",
    '#attributes' => array('class' => array('color')),
  );
  // Fonts Settings .
  $form['future_look_settings_fonts']['settings']['font_css_url'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Font CSS Url'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('font_css_url') != "") ? theme_get_setting('font_css_url') : "https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300italic,400italic,600italic,800italic,700italic,300",
  );
  $form['future_look_settings_fonts']['settings']['font_family'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Font family'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('font_family') != "") ? theme_get_setting('font_family') : "Open Sans",
  );
  $form['future_look_settings_fonts']['settings']['font_size'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Default Font Size'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('font_size') != "") ? theme_get_setting('font_size') : "13px",
  );
  // Social Links  .
  // Facebook link icon .
  $form['future_look_settings_social']['settings']['facebook_link'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Facebook Link'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('facebook_link'),
  );
  // Twitter icon .
  $form['future_look_settings_social']['settings']['twitter_link'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Twitter Link'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('twitter_link'),
  );
  // Linkedin icon.
  $form['future_look_settings_social']['settings']['linkedin_link'] = array(
    '#type'     => 'textfield',
    '#title'    => t('LinkedIn Link'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('linkedin_link'),
  );
  // Gplus icon.
  $form['future_look_settings_social']['settings']['gplus_link'] = array(
    '#type'     => 'textfield',
    '#title'    => t('G+ Link'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('gplus_link'),
  );
  $form['future_look_settings_social']['settings']['social_link_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Social icon Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('social_link_color') != "") ? theme_get_setting('social_link_color') : "FFFFFF",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_social']['settings']['social_link_bgcolor'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Social icon Background-Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('social_link_bgcolor') != "") ? theme_get_setting('social_link_bgcolor') : "3d51ff",
    '#attributes' => array('class' => array('color')),
  );
  // Body color and text color.
  $form['future_look_settings_color']['settings']['body_image'] = array(
    '#type'     => 'managed_file',
    '#title' => t('Body Background Image'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('body_image'),
    '#description' => t('Body Background Image'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['future_look_settings_color']['settings']['body_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Body Baground Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('body_bg_color') != "") ? theme_get_setting('body_bg_color') : "D4FFFF",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_color']['settings']['body_txt_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Body Text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('body_txt_color') != "") ? theme_get_setting('body_txt_color') : "000000",
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_color']['settings']['link_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Links color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('link_color') != "") ? theme_get_setting('link_color') : '9497ff',
    '#attributes' => array('class' => array('color')),
  );
  // Main menu color setting.
  $form['future_look_settings_menu']['settings']['header_color_top'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Main Menu Top Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('header_color_top') != "") ? theme_get_setting('header_color_top') : '243aff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_menu']['settings']['header_color_bottom'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Main Menu Bottom Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('header_color_bottom') != "") ? theme_get_setting('header_color_bottom') : '6e7cff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_menu']['settings']['menu_txt_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Main Menu Text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('menu_txt_color') != "") ? theme_get_setting('menu_txt_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_menu']['settings']['menu_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Main Menu Border Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('menu_border_color') != "") ? theme_get_setting('menu_border_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_menu']['settings']['menu_hvr_sel'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Menu Hover & Selected color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('menu_hvr_sel') != "") ? theme_get_setting('menu_hvr_sel') : 'B061ff',
    '#attributes' => array('class' => array('color')),
  );
  // Tabs color .
  $form['future_look_settings_tabs']['settings']['tabs_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Tabs background'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('tabs_bg_color') != "") ? theme_get_setting('tabs_bg_color') : 'ffb13d',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_tabs']['settings']['tabs_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Tabs text & link color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('tabs_color') != "") ? theme_get_setting('tabs_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_tabs']['settings']['tabs_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Tabs border color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('tabs_border_color') != "") ? theme_get_setting('tabs_border_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  // Footer background color and text color .
  $form['future_look_settings_color_footer']['settings']['footer_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('footer background  Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('footer_bg_color') != "") ? theme_get_setting('footer_bg_color') : '4d4d4d',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_color_footer']['settings']['footer_txt_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('footer text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('footer_txt_color') != "") ? theme_get_setting('footer_txt_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_color_footer']['settings']['footer_link_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('footer link Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('footer_link_color') != "") ? theme_get_setting('footer_link_color') : '4e3bff',
    '#attributes' => array('class' => array('color')),
  );
  // Box1 color settings  .
  $form['future_look_settings_box1_setting']['settings']['box1_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box1 Background color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box1_bg_color') != "") ? theme_get_setting('box1_bg_color') : '000000',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_box1_setting']['settings']['box1_text_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box1 Text color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box1_text_color') != "") ? theme_get_setting('box1_text_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_box1_setting']['settings']['box1_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box1 Border color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box1_border_color') != "") ? theme_get_setting('box1_border_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  // Box2 color settings  .
  $form['future_look_settings_box2_setting']['settings']['box2_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box2 Background color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box2_bg_color') != "") ? theme_get_setting('box2_bg_color') : '6459ff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_box2_setting']['settings']['box2_text_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box2 Text color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box2_text_color') != "") ? theme_get_setting('box2_text_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_box2_setting']['settings']['box2_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box2 Border color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box2_border_color') != "") ? theme_get_setting('box2_border_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  // Box3 color settings .
  $form['future_look_settings_box3_setting']['settings']['box3_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box3 Background color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box3_bg_color') != "") ? theme_get_setting('box3_bg_color') : 'ffab4a',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_box3_setting']['settings']['box3_text_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box3 Text color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box3_text_color') != "") ? theme_get_setting('box3_text_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_box3_setting']['settings']['box3_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Box3 Border color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('box3_border_color') != "") ? theme_get_setting('box3_border_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  // Introduction block text and background color setting.
  $form['future_look_settings_intro']['settings'] = array(
    '#type' => 'container',
  );
  $form['future_look_settings_intro']['settings']['bgimage'] = array(
    '#type'     => 'managed_file',
    '#title' => t('Introduction Field Background Image'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('bgimage'),
    '#description' => t('Introduction-Background image .'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );

  $form['future_look_settings_intro']['settings']['introduction_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Introduction Block Background-Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('introduction_bg_color') != "") ? theme_get_setting('introduction_bg_color') : '000000',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_intro']['settings']['introduction_txt_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Introduction Block Text-Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('introduction_txt_color') != "") ? theme_get_setting('introduction_txt_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  // Front Page image slider setting and image upload setting.
  $form['future_look_settings_slider']['default_slider'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Front Page  Slider'),
    '#default_value' => theme_get_setting('default_slider'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the slider supplied with it.'),
  );
  $form['future_look_settings_slider']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
  // Hide the logo settings when using the default logo.
      'invisible' => array(
        'input[name="default_slider"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['future_look_settings_slider']['settings']['slider_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Slider Background'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('slider_bg_color') != "") ? theme_get_setting('slider_bg_color') : '000000',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_slider']['settings']['front_slide1'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Slider Slide 1'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('front_slide1'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['future_look_settings_slider']['settings']['text_slide1'] = array(
    '#type' => 'textarea',
    '#title'    => t('1 Slide :- Add Some Content in Slide First'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('text_slide1'),
    '#rows' => 5,
    '#resizable' => TRUE,
    '#format' => 'full_html',
  );
  $form['future_look_settings_slider']['settings']['front_slide2'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Slider Slide 2'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('front_slide2'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['future_look_settings_slider']['settings']['text_slide2'] = array(
    '#type' => 'textarea',
    '#title'    => t('2 Slide :-Add Some Content in Slide Second'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('text_slide2'),
    '#rows' => 5,
    '#resizable' => TRUE,
    '#format' => 'full_html',
  );
  $form['future_look_settings_slider']['settings']['front_slide3'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Slider Slide 3'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('front_slide3'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['future_look_settings_slider']['settings']['text_slide3'] = array(
    '#type' => 'textarea',
    '#title'    => t('3 Slide :-Add Some Content in Slide  Third'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('text_slide3'),
    '#rows' => 5,
    '#resizable' => TRUE,
    '#format' => 'full_html',
  );
  $form['future_look_settings_slider']['settings']['front_slide4'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Slider Slide 4'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('front_slide4'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['future_look_settings_slider']['settings']['text_slide4'] = array(
    '#type' => 'textarea',
    '#title'    => t('4 Slide :-Add Some Content in Slide fourth'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('text_slide4'),
    '#rows' => 5,
    '#resizable' => TRUE,
    '#format' => 'full_html',
  );
  $form['future_look_settings_slider']['settings']['front_slide5'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Slider Slide 5'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('front_slide5'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['future_look_settings_slider']['settings']['text_slide5'] = array(
    '#type' => 'textarea',
    '#title'    => t('5 Slide :-Add Some Content in Slide  Fifth'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('text_slide5'),
    '#rows' => 5,
    '#resizable' => TRUE,
    '#format' => 'full_html',
  );
  $form['future_look_settings_slider']['settings']['front_slide6'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Slider Slide 6'),
    '#required' => FALSE,
    '#upload_location' => file_default_scheme() . '://slidertheme/',
    '#default_value' => theme_get_setting('front_slide6'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  $form['future_look_settings_slider']['settings']['text_slide6'] = array(
    '#type' => 'textarea',
    '#title'    => t('6 Slide :-Add Some Content in Slide  Sixth'),
    '#required' => FALSE,
    '#default_value' => theme_get_setting('text_slide6'),
    '#rows' => 5,
    '#resizable' => TRUE,
    '#format' => 'full_html',
  );
  $theme_settings_path = drupal_get_path('theme', 'future_look') . '/theme-settings.php';
  if (file_exists($theme_settings_path) && !in_array($theme_settings_path, $form_state['build_info']['files'])) {
    $form_state['build_info']['files'][] = $theme_settings_path;
  }
  // Button setting .
  $form['future_look_settings_button']['button_bg_hd_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Button Top-Background Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('button_bg_hd_color') != "") ? theme_get_setting('button_bg_hd_color') : '000000',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_button']['button_bg_ft_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Button Bottom-Background Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('button_bg_ft_color') != "") ? theme_get_setting('button_bg_ft_color') : '3b3b3b',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_button']['button_txt_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Button Text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('button_txt_color') != "") ? theme_get_setting('button_txt_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_button']['button_hover_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Button Hover Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('button_hover_color') != "") ? theme_get_setting('button_hover_color') : 'ff004c',
    '#attributes' => array('class' => array('color')),
  );
  // Right Bar  Color setting  .
  $form['future_look_settings_right_bar_color']['settings']['rightbar_text_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('RightBar  text color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('rightbar_text_color') != "") ? theme_get_setting('rightbar_text_color') : 'ffffff',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_right_bar_color']['settings']['rightbar_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('RightBar Background  color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('rightbar_bg_color') != "") ? theme_get_setting('rightbar_bg_color') : '404040',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_right_bar_color']['settings']['rightbar_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('RightBar Border color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('rightbar_border_color') != "") ? theme_get_setting('rightbar_border_color') : 'b5b5b5',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_right_bar_color']['settings']['rightbar_link_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('RightBar link color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('rightbar_link_color') != "") ? theme_get_setting('rightbar_link_color') : 'bda29b',
    '#attributes' => array('class' => array('color')),
  );
  // Content Bottom color setting .
  $form['future_look_settings_content_bottom_color']['settings']['content_bottom_text_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Content Bottom Region Text Color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('content_bottom_text_color') != "") ? theme_get_setting('content_bottom_text_color') : 'fcfcfc',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_content_bottom_color']['settings']['content_bottom_bg_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Content Bottom Region Background  color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('content_bottom_bg_color') != "") ? theme_get_setting('content_bottom_bg_color') : 'ffa245',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_content_bottom_color']['settings']['content_bottom_link_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Content Bottom Region link color'),
    '#required' => FALSE,
    '#default_value' => (
  theme_get_setting('content_bottom_link_color') != "") ? theme_get_setting('content_bottom_link_color') : '000000',
    '#attributes' => array('class' => array('color')),
  );
  $form['future_look_settings_content_bottom_color']['settings']['content_bottom_border_color'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Content Bottom Region Border color'),
    '#required' => FALSE,
    '#default_value' => (theme_get_setting('content_bottom_border_color') != "") ? theme_get_setting('content_bottom_border_color') : 'ffd073',
    '#attributes' => array('class' => array('color')),
  );
  $form_elements = element_children($form);
  foreach ($form_elements as $element) {
    if ($form[$element]['#type'] == 'fieldset') {
      // Identify fieldsets and collapse them.
      $form[$element]['#collapsible'] = TRUE;
      $form[$element]['#collapsed']   = TRUE;
    }
  }
  $form['#validate'][] = 'future_look_form_system_theme_settings_validate';
  $form['#submit'][] = 'future_look_form_submit';
}
// Front page slider Image upload and save permanetly .
// In this future_look_form_submit() function all
// Theme-setting fields value get and create future_look.css
// File and also save uploaded images with status 1 in
// Database.
/**
 * Implements hook_form_submit() for future_look_form_submit().
 */
function future_look_form_submit(&$form, &$form_state) {
  // Load the file via file.fid.
  future_look_save_images($form_state['values']['front_slide1']);
  future_look_save_images($form_state['values']['front_slide2']);
  future_look_save_images($form_state['values']['front_slide3']);
  future_look_save_images($form_state['values']['front_slide4']);
  future_look_save_images($form_state['values']['front_slide5']);
  future_look_save_images($form_state['values']['front_slide6']);
  future_look_save_images($form_state['values']['bgimage']);
  future_look_save_images($form_state['values']['body_image']);
  // ##############################################################################################.
  $slider_bg_color = $form_state['values']['slider_bg_color'];
  $tbl_odd_row_color = $form_state['values']['odd_row_color'];
  $tbl_odd_row_bg = $form_state['values']['odd_row_bg'];
  $tbl_odd_row_link = $form_state['values']['odd_row_link'];
  $tbl_even_row_color = $form_state['values']['even_row_color'];
  $tbl_even_row_bg = $form_state['values']['even_row_bg'];
  $tbl_even_row_link = $form_state['values']['even_row_link'];
  $tbl_border_color = $form_state['values']['table_border_color'];
  $tbl_thead_color = $form_state['values']['thead_color'];
  $tbl_thead_bg = $form_state['values']['thead_bg'];
  $tbl_thead_link = $form_state['values']['thead_link'];
  $body_bg_color = $form_state['values']['body_bg_color'];
  $body_txt_color = $form_state['values']['body_txt_color'];
  $link_color = $form_state['values']['link_color'];
  $headercolor_top = $form_state['values']['header_color_top'];
  $headercolor_bottom = $form_state['values']['header_color_bottom'];
  $menu_text_color = $form_state['values']['menu_txt_color'];
  $menu_hvr_sel = $form_state['values']['menu_hvr_sel'];
  $menu_border_color = $form_state['values']['menu_border_color'];
  $footer_bg_color = $form_state['values']['footer_bg_color'];
  $footer_txt_color = $form_state['values']['footer_txt_color'];
  $footer_link_color = $form_state['values']['footer_link_color'];
  $tabs_bg_color = $form_state['values']['tabs_bg_color'];
  $tabs_color = $form_state['values']['tabs_color'];
  $tabs_border_color = $form_state['values']['tabs_border_color'];
  $box1_bg_color = $form_state['values']['box1_bg_color'];
  $box1_text_color = $form_state['values']['box1_text_color'];
  $box1_border_color = $form_state['values']['box1_border_color'];
  $box2_bg_color = $form_state['values']['box2_bg_color'];
  $box2_text_color = $form_state['values']['box2_text_color'];
  $box2_border_color = $form_state['values']['box2_border_color'];
  $box3_bg_color = $form_state['values']['box3_bg_color'];
  $box3_text_color = $form_state['values']['box3_text_color'];
  $box3_border_color = $form_state['values']['box3_border_color'];
  $intro_bg_color = $form_state['values']['introduction_bg_color'];
  $intro_txt_color = $form_state['values']['introduction_txt_color'];
  $social_icon_social_color = $form_state['values']['social_link_color'];
  $social_icon_social_bgcolor = $form_state['values']['social_link_bgcolor'];
  $right_bar_text_color = $form_state['values']['rightbar_text_color'];
  $right_bar_bg_color = $form_state['values']['rightbar_bg_color'];
  $right_bar_link_color = $form_state['values']['rightbar_link_color'];
  $right_bar_border_color = $form_state['values']['rightbar_border_color'];
  $content_bottom_link_color = $form_state['values']['content_bottom_link_color'];
  $content_bottom_bg_color = $form_state['values']['content_bottom_bg_color'];
  $content_bottom_text_color = $form_state['values']['content_bottom_text_color'];
  $content_bottom_border_color = $form_state['values']['content_bottom_border_color'];
  $btn_bg_hd_color = $form_state['values']['button_bg_hd_color'];
  $btn_bg_ft_color = $form_state['values']['button_bg_ft_color'];
  $btn_txt_color = $form_state['values']['button_txt_color'];
  $btn_hover_color = $form_state['values']['button_hover_color'];
  $font_family = $form_state['values']['font_family'];
  $font_size = $form_state['values']['font_size'];
  $intro_bg_image_url = "";
  $intro_background_image = $form_state['values']['bgimage'];
  if ((isset($intro_background_image)) && ($intro_background_image != "")) {
    $intro_bg_image_url = file_create_url(file_load($intro_background_image)->uri);
  }
  $body_bg_image_url = "";
  $body_bg_image = $form_state['values']['body_image'];
  if ((isset($body_bg_image)) && ($body_bg_image != "")) {
    $body_bg_image_url = file_create_url(file_load($body_bg_image)->uri);
  }
  $stylesheet = '';
  $stylesheet .= '
  a{
    color:#' . $link_color . ';
  }
  body {
    background:#' . $body_bg_color . ' repeat url(' . $body_bg_image_url . ');
    color:#' . $body_txt_color . ';
    font-family:' . $font_family . ';
    font-size:' . $font_size . ';
  }';
  $stylesheet .= "
  .dropdown-menu{
    background-color:#" . $menu_text_color . ";
    font-size:" . $font_size . " ;
  }
  ";
  $stylesheet .= '
  .navbar-inverse{
    color:#' . $menu_text_color . ';
    background:#' . $headercolor_top . ';
    background: -webkit-linear-gradient( #' . $headercolor_top . '
    , #' . $headercolor_bottom . '  ); 
  }
  .nav-tabs,nav-tabs li a{
    border-color:#' . $tabs_border_color . ';
  }
  .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
    background-color:#' . $tabs_bg_color . ';
    color:#' . $tabs_color . ';
  }
  .nav > li > a:focus, .nav > li > a:hover {
    background-color:#' . $tabs_bg_color . ';
    color:#' . $tabs_color . ';
    text-decoration: none;
  }

  @supports(background-image: linear-gradient(to bottom, #' . $headercolor_top;
  $stylesheet .= '  0px, #' . $headercolor_bottom . '  100%)) {
    .navbar-inverse{
      background-image: linear-gradient(to bottom, #' . $headercolor_top;
  $stylesheet .= '  0px, #' . $headercolor_bottom . '  100%);
      background-repeat: repeat-x;
    }
  }
  .footer-background {
    background-color: #' . $footer_bg_color . ';
    height: auto;
    color:#' . $footer_txt_color . ';
  }
  .footer-background a{
  color:#' . $footer_link_color . ';}
  
  .navbar-inverse .navbar-nav  li  a{
    color: #' . $menu_text_color . ';
  }
  .menu-text-color.agri-nav-item.nav-space{
    color: #' . $menu_text_color . ';
  }
  .dropdown-menu li  a{
    color:#' . $menu_text_color . ';
    background-image: linear-gradient(to bottom, #' . $headercolor_top . '  0px, #' . $headercolor_bottom . '  100%);
  background-repeat: repeat-x;}
  .dropdown-menu  li  a:focus, .dropdown-menu  li  a:hover{
    color:white;
    background:none;
  }
  .dropdown-menu{
    background-color: #' . $headercolor_top . ' ;
    background-image: linear-gradient(to bottom, #' . $headercolor_top . '  0px, #' . $headercolor_bottom . '  100%);
	background: -webkit-linear-gradient(#' . $headercolor_top . '
    , #' . $headercolor_bottom . ');
    background-repeat: repeat-x;
  } 
  .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .open > a,.navbar-inverse .navbar-nav > :hover > a, .navbar-inverse .navbar-nav > li > a:hover, .navbar-nav ul.dropdown-menu li a:hover,  .navbar-nav ul.dropdown-menu li:hover, ul.dropdown-menu li a:hover {
    background-image:none;
    background-color:#' . $menu_hvr_sel . ";
    box-shadow:none;
	color:#" . $menu_text_color . ";
    border-width:0px;
    border-color:#" . $menu_hvr_sel . ";
    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#" . $menu_hvr_sel . "', endColorstr='#" . $menu_hvr_sel . "', GradientType=0); BACKGROUND-REPEAT: repeat-x
  }
  
  .dropdown-menu > .active > a, .dropdown-menu > .active > a:focus, .dropdown-menu > .active > a:hover{
    background-image:none;
    background-color:#" . $menu_hvr_sel . ";
    box-shadow:none;
    border-width:0px;
  }
  
  
  .dropdown-menu > .active, .dropdown-menu > .active:focus, .dropdown-menu > .active:hover,.dropdown-menu > li:hover{
    background-image:none;
    background-color:#" . $menu_hvr_sel . ";
    box-shadow:none;
    border-width:0px;
    border-color:#" . $menu_hvr_sel . ";
  }
  
  
  .dropdown-menu li a{
    background-image:none;
  }
  
  .navbar-inverse .navbar-brand{
    color:#" . $menu_text_color . ";
  }
  
  .navbar-inverse .navbar-nav .open .dropdown-menu > li > a{
    color:#" . $menu_text_color . ";
  }
  
  .navbar-inverse{
    border-color:#" . $menu_border_color . ";
  }
  
  .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:focus, .navbar-inverse .navbar-nav > .active > a:hover {
    background-color:#" . $menu_hvr_sel . ";
    color: #fff;
  }
  
  .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:focus, .navbar-inverse .navbar-nav > .open > a:hover {
    background-color:#" . $menu_hvr_sel . ";
    color: #fff;
  }
  
  #box-first .region{
    background-color:#" . $box1_bg_color . ";
    color:#" . $box1_text_color . ";
    border:#" . $box1_border_color . " 1px solid;
  }
  
  #box-second .region{
    background-color:#" . $box2_bg_color . ";
    color:#" . $box2_text_color . ";
    border:#" . $box2_border_color . " 1px solid;
  }
  
  #box-third .region{
    background-color:#" . $box3_bg_color . ";
    color:#" . $box3_text_color . ";
    border:#" . $box3_border_color . " 1px solid;
  }
  
  
  
  #introduction {
    color:#" . $intro_txt_color . ";
    background:#" . $intro_bg_color . " url(" . $intro_bg_image_url . ");
  }
  
  #right_social_bar a.btn-social-icon{
    background-color:#" . $social_icon_social_bgcolor . ";
    color:#" . $social_icon_social_color . ";
    
  }
  
  #rightbar {
    background-color: #" . $right_bar_bg_color . ";
    color: #" . $right_bar_text_color . ";
    border:#" . $right_bar_border_color . "  1px solid;
  }
  
  #rightbar a{
    color :#" . $right_bar_link_color . ";
  }
  
  
  #content_bottom
  {
    background-color:#" . $content_bottom_bg_color . ";
    color:#" . $content_bottom_text_color . ";
    border-top:#" . $content_bottom_border_color . "  1px solid;
    border-bottom:#" . $content_bottom_border_color . "  1px solid;
  }
  #content_bottom a
  {
    color:#" . $content_bottom_link_color . ";
    
  }
  #rightbar{
    margin-bottom: 15px;
    margin-top: 15px;
  }
  .btn{
  margin-right:5px;}
  
  tr.even, tr.odd {
    border-bottom: 1px solid #" . $tbl_border_color . ";
    padding: 0.1em 0.6em;
  }
  
  tr.even,table > tbody > tr.even > td.active {
    background-color: #" . $tbl_even_row_bg . ";
    color: #" . $tbl_even_row_color . ";
  }
  
  tr.odd,table > tbody > tr.odd > td.active {
    background-color: #" . $tbl_odd_row_bg . ";
    color: #" . $tbl_odd_row_color . ";
  }
  
  .table > thead > tr > th, .table > thead > tr > th.active {
    background-color: #" . $tbl_thead_bg . ";
    color: #" . $tbl_thead_color . ";
  }
  table tr th {
    background-color: #" . $tbl_thead_bg . ";
    color: #" . $tbl_thead_color . ";
  }
  .table > thead > tr > th > a{
    color : #" . $tbl_thead_link . " ;
  }
  
  .table > tbody > tr.odd > td  a {
    color : #" . $tbl_odd_row_link . " ;
  }
  
  .table > tbody > tr.even > td  a {
    color : #" . $tbl_even_row_link . " ;
  }
  
  
  .btn-custom{
    background-color:#" . $btn_bg_hd_color . ";
    background-image: linear-gradient(to bottom, #" . $btn_bg_hd_color . "
    0px, #" . $btn_bg_ft_color . "   100%);
    
	background: -webkit-linear-gradient(#" . $btn_bg_hd_color . "
    , #" . $btn_bg_ft_color . "); 

    color:#" . $btn_txt_color . ";
    
    border:#" . $btn_bg_ft_color . " 1px solid;
  }
  
  .btn-custom:hover,.btn-custom:focus,.btn-custom.active{
    background-color:#" . $btn_hover_color . ";
    color:#" . $btn_txt_color . ";
    background-image:none;
    border:#" . $btn_hover_color . " 1px solid;
  }
  
  .carousel .item{
    background-color:#" . $slider_bg_color . ";
  }

  @media(max-width: 767px){

.navbar-inverse .navbar-nav .open .dropdown-menu > .active > a, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:focus, .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:hover {
    background-color:#" . $menu_hvr_sel . ";
    color: #fff;
}
}
  
  ";
  file_unmanaged_delete("public://future_look.css");
  file_unmanaged_save_data($stylesheet, "public://future_look.css", "FILE_EXISTS_REPLACE");
}
/**
 * Implements hook_form_system_theme_settings_validate().
 */
function future_look_form_system_theme_settings_validate($form, &$form_state) {

  if (($form_state['values']['default_slider'] == 1)&&($form_state['values']['front_slide1'] == '')) {
    form_set_error('title', t('Slide1 is require for slider.'));
  }
}
// Custom function for image save .
/**
 * Future_look_save_images().
 */
function future_look_save_images($path) {
  global $user;
  if (isset($path)&&$path != 0) {
    $image = file_load($path);
    $image->status = FILE_STATUS_PERMANENT;
    file_save($image);
    file_usage_add($image, 'user', 'user', $user->uid);
  }
}
