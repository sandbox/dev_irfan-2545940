/**
 * @file
 * Modified jscolor.js for color picker.
 */

 /* jshint -W054 */

  var jscolor = {

    dir: '',
    bindClass: 'color',
    binding: true,
    preloading: true,


    install: function () {
      "use strict";
      jscolor.addEvent(window, 'load', jscolor.init);
    },


    init: function () {
      "use strict";
      if (jscolor.binding) {
        jscolor.bind();
      }
      if (jscolor.preloading) {
        jscolor.preload();
      }
    },


    getDir: function () {
      "use strict";
      if (!jscolor.dir) {
        var detected = jscolor.detectDir();
        jscolor.dir = detected !== false ? detected : 'jscolor/';
      }
      return jscolor.dir;
    },


    detectDir: function () {
      "use strict";
      var base = location.href;

      var e = document.getElementsByTagName('base');
      var i;
      for (i = 0; i < e.length; i += 1) {
        if (e[i].href) {
          base = e[i].href;
        }
      }

      e = document.getElementsByTagName('script');
      for (i = 0; i < e.length; i += 1) {
        if (e[i].src && /(^|\/)jscolor\.js([?#].*)?$/i.test(e[i].src)) {
          var src = new jscolor.URI(e[i].src);
          var srcAbs = src.toAbsolute(base);
          srcAbs.path = srcAbs.path.replace(/[^\/]+$/, '');
          srcAbs.query = null;
          srcAbs.fragment = null;
          return srcAbs.toString();
        }
      }
      return false;
    },


    bind: function () {
      "use strict";
      var matchClass = new RegExp('(^|\\s)(' + jscolor.bindClass + ')(\\s*(\\{[^}]*\\})|\\s|$)', 'i');
      var e = document.getElementsByTagName('input');
      var i;
      for (i = 0; i < e.length; i += 1) {
        if (jscolor.isColorAttrSupported && e[i].type.toLowerCase() === 'color') {
          // Skip inputs of type 'color' if the browser supports this feature.
          continue;
        }
        var m;
        if (!e[i].color && e[i].className && (m = e[i].className.match(matchClass))) {
          var prop = {};
          if (m[4]) {
            try {
              prop = m[4];
            }
            catch (eInvalidProp) {
              // Empty.
            }
          }
          e[i].color = new jscolor.color(e[i], prop);
        }
      }
    },


    preload: function () {
      "use strict";
      for (var fn in jscolor.imgRequire) {
        if (jscolor.imgRequire.hasOwnProperty(fn)) {
          jscolor.loadImage(fn);
        }
      }
    },


    images: {
      pad: [181, 101],
      sld: [16, 101],
      cross: [15, 15],
      arrow: [7, 11]
    },


    imgRequire: {},
    imgLoaded: {},


    requireImage: function (filename) {
      "use strict";
      jscolor.imgRequire[filename] = true;
    },


    loadImage: function (filename) {
      "use strict";
      if (!jscolor.imgLoaded[filename]) {
        jscolor.imgLoaded[filename] = new Image();
        jscolor.imgLoaded[filename].src = jscolor.getDir() + filename;
      }
    },


    fetchElement: function (mixed) {
      "use strict";
      return typeof mixed === 'string' ? document.getElementById(mixed) : mixed;
    },


    addEvent: function (el, evnt, func) {
      "use strict";
      if (el.addEventListener) {
        el.addEventListener(evnt, func, false);
      }
      else if (el.attachEvent) {
        el.attachEvent('on' + evnt, func);
      }
    },


    fireEvent: function (el, evnt) {
      "use strict";
      if (!el) {
        return;
      }
      var ev;
      if (document.createEvent) {
        ev = document.createEvent('HTMLEvents');
        ev.initEvent(evnt, true, true);
        el.dispatchEvent(ev);
      }
      else if (document.createEventObject) {
        ev = document.createEventObject();
        el.fireEvent('on' + evnt, ev);
      }
      else if (el['on' + evnt]) {
        el['on' + evnt]();
      }
    },


    getElementPos: function (e) {
      "use strict";
      var e1 = e; var e2 = e;
      var x = 0; var y = 0;
      if (e1.offsetParent) {
        do {
          x += e1.offsetLeft;
          y += e1.offsetTop;
        } while ((e1 = e1.offsetParent));
      }
      while ((e2 = e2.parentNode) && e2.nodeName.toUpperCase() !== 'BODY') {
        x -= e2.scrollLeft;
        y -= e2.scrollTop;
      }
      return [x, y];
    },


    getElementSize: function (e) {
      "use strict";
      return [e.offsetWidth, e.offsetHeight];
    },


    getRelMousePos: function (e) {
      "use strict";
      var x = 0; var y = 0;
      if (!e) {
        e = window.event;
      }
      if (typeof e.offsetX === 'number') {
        x = e.offsetX;
        y = e.offsetY;
      }
      else if (typeof e.layerX === 'number') {
        x = e.layerX;
        y = e.layerY;
      }
      return {
        x: x, y: y
      };
    },


    getViewPos: function () {
      "use strict";
      if (typeof window.pageYOffset === 'number') {
        return [window.pageXOffset, window.pageYOffset];
      }
      else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
        return [document.body.scrollLeft, document.body.scrollTop];
      }
      else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
        return [document.documentElement.scrollLeft, document.documentElement.scrollTop];
      }
      else {
        return [0, 0];
      }
    },


    getViewSize: function () {
      "use strict";
      if (typeof window.innerWidth === 'number') {
        return [window.innerWidth, window.innerHeight];
      }
      else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        return [document.body.clientWidth, document.body.clientHeight];
      }
      else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        return [document.documentElement.clientWidth, document.documentElement.clientHeight];
      }
      else {
        return [0, 0];
      }
    },


    URI: function (uri) {
      "use strict";
      this.scheme = null;
      this.authority = null;
      this.path = '';
      this.query = null;
      this.fragment = null;

      this.parse = function (uri) {
        var m = uri.match(/^(([A-Za-z][0-9A-Za-z+.-]*)(:))?((\/\/)([^\/?#]*))?([^?#]*)((\?)([^#]*))?((#)(.*))?/);
        this.scheme = m[3] ? m[2] : null;
        this.authority = m[5] ? m[6] : null;
        this.path = m[7];
        this.query = m[9] ? m[10] : null;
        this.fragment = m[12] ? m[13] : null;
        return this;
      };

      this.toString = function () {
        var result = '';
        if (this.scheme !== null) {
          result = result + this.scheme + ':';
        }
        if (this.authority !== null) {
          result = result + '//' + this.authority;
        }
        if (this.path !== null) {
          result = result + this.path;
        }
        if (this.query !== null) {
          result = result + '?' + this.query;
        }
        if (this.fragment !== null) {
          result = result + '#' + this.fragment;
        }
        return result;
      };

      this.toAbsolute = function (base) {
        base = new jscolor.URI(base);
        var r = this;
        var t = new jscolor.URI();

        if (base.scheme === null) {
          return false;
        }

        if (r.scheme !== null && r.scheme.toLowerCase() === base.scheme.toLowerCase()) {
          r.scheme = null;
        }

        if (r.scheme !== null) {
          t.scheme = r.scheme;
          t.authority = r.authority;
          t.path = remove_dot_segments(r.path);
          t.query = r.query;
        }
        else {
          if (r.authority !== null) {
            t.authority = r.authority;
            t.path = remove_dot_segments(r.path);
            t.query = r.query;
          }
          else {
            if (r.path === '') {
              t.path = base.path;
              if (r.query !== null) {
                t.query = r.query;
              }
              else {
                t.query = base.query;
              }
            }
            else {
              if (r.path.substr(0, 1) === '/') {
                t.path = remove_dot_segments(r.path);
              }
              else {
                if (base.authority !== null && base.path === '') {
                  t.path = '/' + r.path;
                }
                else {
                  t.path = base.path.replace(/[^\/]+$/, '') + r.path;
                }
                t.path = remove_dot_segments(t.path);
              }
              t.query = r.query;
            }
            t.authority = base.authority;
          }
          t.scheme = base.scheme;
        }
        t.fragment = r.fragment;

        return t;
      };

      function remove_dot_segments(path) {
        var out = '';
        while (path) {
          if (path.substr(0, 3) === '../' || path.substr(0, 2) === './') {
            path = path.replace(/^\.+/, '').substr(1);
          }
          else if (path.substr(0, 3) === '/./' || path === '/.') {
            path = '/' + path.substr(3);
          }
          else if (path.substr(0, 4) === '/../' || path === '/..') {
            path = '/' + path.substr(4);
            out = out.replace(/\/?[^\/]*$/, '');
          }
          else if (path === '.' || path === '..') {
            path = '';
          }
          else {
            var rm = path.match(/^\/?[^\/]*/)[0];
            path = path.substr(rm.length);
            out = out + rm;
          }
        }
        return out;
      }

      if (uri) {
        this.parse(uri);
      }

    },


    color: function (target, prop) {
      "use strict";
      this.required = true;
      this.adjust = true;
      this.hash = false;
      this.caps = true;
      this.slider = true;
      this.valueElement = target;
      this.styleElement = target;
      this.onImmediateChange = null;
      this.hsv = [0, 0, 1];
      this.rgb = [1, 1, 1];
      this.minH = 0;
      this.maxH = 6;
      this.minS = 0;
      this.maxS = 1;
      this.minV = 0;
      this.maxV = 1;

      this.pickerOnfocus = true;
      this.pickerMode = 'HSV';
      this.pickerPosition = 'bottom';
      this.pickerSmartPosition = true;
      this.pickerButtonHeight = 20;
      this.pickerClosable = false;
      this.pickerCloseText = 'Close';
      this.pickerButtonColor = 'ButtonText';
      this.pickerFace = 10;
      this.pickerFaceColor = 'ThreeDFace';
      this.pickerBorder = 1;
      this.pickerBorderColor = 'ThreeDHighlight ThreeDShadow ThreeDShadow ThreeDHighlight';
      this.pickerInset = 1;
      this.pickerInsetColor = 'ThreeDShadow ThreeDHighlight ThreeDHighlight ThreeDShadow';
      this.pickerZIndex = 10000;

      for (var p in prop) {
        if (prop.hasOwnProperty(p)) {
          this[p] = prop[p];
        }
      }

      this.hidePicker = function () {
        if (is_picker_owner()) {
          remove_picker();
        }
      };

      this.showPicker = function () {
        if (!is_picker_owner()) {
          var tp = jscolor.getElementPos(target);
          var ts = jscolor.getElementSize(target);
          var vp = jscolor.getViewPos();
          var vs = jscolor.getViewSize();
          var ps = get_picker_dims(this);
          var a; var b; var c;
          switch (this.pickerPosition.toLowerCase()) {
            case 'left':
              a = 1; b = 0; c = -1;
              break;

            case 'right':
              a = 1; b = 0; c = 1;
              break;

            case 'top':
              a = 0; b = 1; c = -1;
              break;

            default:
              a = 0; b = 1; c = 1;
              break;

          }
          var l = (ts[b] + ps[b]) / 2;
          var pp;
          if (!this.pickerSmartPosition) {
            pp = [tp[a], tp[b] + ts[b] - l + l * c];
          }
          else {
            var exp1 = (-vp[a] + tp[a] + ts[a] / 2 > vs[a] / 2 && tp[a] + ts[a] - ps[a] >= 0 ? tp[a] + ts[a] - ps[a] : tp[a]);
            var exp2 = -vp[b] + tp[b] + ts[b] / 2 > vs[b] / 2 && tp[b] + ts[b] - l - l * c >= 0 ? tp[b] + ts[b] - l - l * c : tp[b] + ts[b] - l + l * c;
            var exp3 = tp[b] + ts[b] - l + l * c >= 0 ? tp[b] + ts[b] - l + l * c : tp[b] + ts[b] - l - l * c;
            pp = [
              -vp[a] + tp[a] + ps[a] > vs[a] ? exp1 : tp[a],
              -vp[b] + tp[b] + ts[b] + ps[b] - l + l * c > vs[b] ? exp2 : exp3
            ];
          }
          draw_picker(pp[a], pp[b]);
        }
      };

      this.importColor = function () {
        if (!valueElement) {
          this.exportColor();
        }
        else {
          if (!this.adjust) {
            if (!this.fromString(valueElement.value, leaveValue)) {
              styleElement.style.backgroundImage = styleElement.jscStyle.backgroundImage;
              styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
              styleElement.style.color = styleElement.jscStyle.color;
              this.exportColor(leaveValue | leaveStyle);
            }
          }
          else if (!this.required && /^\s*$/.test(valueElement.value)) {
            valueElement.value = '';
            styleElement.style.backgroundImage = styleElement.jscStyle.backgroundImage;
            styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
            styleElement.style.color = styleElement.jscStyle.color;
            this.exportColor(leaveValue | leaveStyle);

          }
          else if (this.fromString(valueElement.value)) {
            // OK.
          }
          else {
            this.exportColor();
          }
        }
      };

      this.exportColor = function (flags) {
        if (!(flags & leaveValue) && valueElement) {
          var value = this.toString();
          if (this.caps) {
            value = value.toUpperCase();
          }
          if (this.hash) {
            value = '#' + value;
          }
          valueElement.value = value;
        }
        if (!(flags & leaveStyle) && styleElement) {
          styleElement.style.backgroundImage = "none";
          styleElement.style.backgroundColor =
          '#' + this.toString();
          styleElement.style.color =
          0.213 * this.rgb[0] +
          0.715 * this.rgb[1] +
          0.072 * this.rgb[2]
          < 0.5 ? '#FFF' : '#000';
        }
        if (!(flags & leavePad) && is_picker_owner()) {
          redraw_pad();
        }
        if (!(flags & leaveSld) && is_picker_owner()) {
          redraw_sld();
        }
      };

      this.fromHSV = function (h, s, v, flags) {
        if (h !== null) {
          h = Math.max(0.0, this.minH, Math.min(6.0, this.maxH, h));
        }
        if (s !== null) {
          s = Math.max(0.0, this.minS, Math.min(1.0, this.maxS, s));
        }
        if (v !== null) {
          v = Math.max(0.0, this.minV, Math.min(1.0, this.maxV, v));
        }

        this.rgb = HSV_RGB(
        h === null ? this.hsv[0] : (this.hsv[0] = h),
        s === null ? this.hsv[1] : (this.hsv[1] = s),
        v === null ? this.hsv[2] : (this.hsv[2] = v)
        );

        this.exportColor(flags);
      };

      this.fromRGB = function (r, g, b, flags) {
        if (r !== null) {
          r = Math.max(0.0, Math.min(1.0, r));
        }
        if (g !== null) {
          g = Math.max(0.0, Math.min(1.0, g));
        }
        if (b !== null) {
          b = Math.max(0.0, Math.min(1.0, b));
        }

        var hsv = RGB_HSV(
        r === null ? this.rgb[0] : r,
        g === null ? this.rgb[1] : g,
        b === null ? this.rgb[2] : b
        );
        if (hsv[0] !== null) {
          this.hsv[0] = Math.max(0.0, this.minH, Math.min(6.0, this.maxH, hsv[0]));
        }
        if (hsv[2] !== 0) {
          this.hsv[1] = hsv[1] === null ? null : Math.max(0.0, this.minS, Math.min(1.0, this.maxS, hsv[1]));
        }
        this.hsv[2] = hsv[2] === null ? null : Math.max(0.0, this.minV, Math.min(1.0, this.maxV, hsv[2]));

        var rgb = HSV_RGB(this.hsv[0], this.hsv[1], this.hsv[2]);
        this.rgb[0] = rgb[0];
        this.rgb[1] = rgb[1];
        this.rgb[2] = rgb[2];

        this.exportColor(flags);
      };

      this.fromString = function (hex, flags) {
        var m = hex.match(/^\W*([0-9A-F]{3}([0-9A-F]{3})?)\W*$/i);
        if (!m) {
          return false;
        }
        else {
          if (m[1].length === 6) {
            this.fromRGB(
            parseInt(m[1].substr(0, 2), 16) / 255,
            parseInt(m[1].substr(2, 2), 16) / 255,
            parseInt(m[1].substr(4, 2), 16) / 255,
            flags
            );
          }
          else {
            this.fromRGB(
            parseInt(m[1].charAt(0) + m[1].charAt(0), 16) / 255,
            parseInt(m[1].charAt(1) + m[1].charAt(1), 16) / 255,
            parseInt(m[1].charAt(2) + m[1].charAt(2), 16) / 255,
            flags
            );
          }
          return true;
        }
      };

      this.toString = function () {
        return (
        (0x100 | Math.round(255 * this.rgb[0])).toString(16).substr(1) +
        (0x100 | Math.round(255 * this.rgb[1])).toString(16).substr(1) +
        (0x100 | Math.round(255 * this.rgb[2])).toString(16).substr(1)
        );
      };

      function RGB_HSV(r, g, b) {
        var n = Math.min(Math.min(r, g), b);
        var v = Math.max(Math.max(r, g), b);
        var m = v - n;
        if (m === 0) {
          return [null, 0, v];
        }
        var s_exp1 = (g === n ? 5 + (r - b) / m : 1 + (g - r) / m);
        var h = r === n ? 3 + (b - g) / m : s_exp1;
        return [h === 6 ? 0 : h, m / v, v];
      }

      function HSV_RGB(h, s, v) {
        if (h === null) {
          return [v, v, v];
        }
        var i = Math.floor(h);
        var f = i % 2 ? h - i : 1 - (h - i);
        var m = v * (1 - s);
        var n = v * (1 - s * f);
        switch (i) {
          case 6:

          case 0: return [v, n, m];

          case 1: return [n, v, m];

          case 2: return [m, v, n];

          case 3: return [m, n, v];

          case 4: return [n, m, v];

          case 5: return [v, m, n];

        }
      }

      function remove_picker() {
        delete jscolor.picker.owner;
        document.getElementsByTagName('body')[0].removeChild(jscolor.picker.boxB);
      }

      function draw_picker(x, y) {
        if (!jscolor.picker) {
          jscolor.picker = {
            box: document.createElement('div'),
            boxB: document.createElement('div'),
            pad: document.createElement('div'),
            padB: document.createElement('div'),
            padM: document.createElement('div'),
            sld: document.createElement('div'),
            sldB: document.createElement('div'),
            sldM: document.createElement('div'),
            btn: document.createElement('div'),
            btnS: document.createElement('span'),
            btnT: document.createTextNode(THIS.pickerCloseText)
          };
          var i; var segSize;
          for (i = 0, segSize = 4; i < jscolor.images.sld[1]; i += segSize) {
            var seg = document.createElement('div');
            seg.style.height = segSize + 'px';
            seg.style.fontSize = '1px';
            seg.style.lineHeight = '0';
            jscolor.picker.sld.appendChild(seg);
          }
          jscolor.picker.sldB.appendChild(jscolor.picker.sld);
          jscolor.picker.box.appendChild(jscolor.picker.sldB);
          jscolor.picker.box.appendChild(jscolor.picker.sldM);
          jscolor.picker.padB.appendChild(jscolor.picker.pad);
          jscolor.picker.box.appendChild(jscolor.picker.padB);
          jscolor.picker.box.appendChild(jscolor.picker.padM);
          jscolor.picker.btnS.appendChild(jscolor.picker.btnT);
          jscolor.picker.btn.appendChild(jscolor.picker.btnS);
          jscolor.picker.box.appendChild(jscolor.picker.btn);
          jscolor.picker.boxB.appendChild(jscolor.picker.box);
        }

        var p = jscolor.picker;

        // Controls interaction.
        p.box.onmouseup =
        p.box.onmouseout = function () {
          target.focus();
        };
        p.box.onmousedown = function () {
          abortBlur = true;
        };
        p.box.onmousemove = function (e) {
          if (holdPad || holdSld) {
            // holdPad && set_pad(e);
            // holdSld && set_sld(e);
            if (holdPad) {
              set_pad(e);
            }
            if (holdSld) {
              set_sld(e);
            }
            if (document.selection) {
              document.selection.empty();
            }
            else if (window.getSelection) {
              window.getSelection().removeAllRanges();
            }
            dispatch_immediate_change();
          }
        };
        if ('ontouchstart' in window) {
          var handle_touchmove = function (e) {
            var event = {
              offsetX: e.touches[0].pageX - touchOffset.X,
              offsetY: e.touches[0].pageY - touchOffset.Y
            };
            if (holdPad || holdSld) {
              if (holdPad) {
                set_pad(event);
              }
              if (holdSld) {
                set_sld(event);
              }
              dispatch_immediate_change();
            }
            e.stopPropagation();
            e.preventDefault();
          };
          p.box.removeEventListener('touchmove', handle_touchmove, false);
          p.box.addEventListener('touchmove', handle_touchmove, false);
        }
        p.padM.onmouseup =
        p.padM.onmouseout = function () {
          if (holdPad) {
            holdPad = false; jscolor.fireEvent(valueElement, 'change');
          }
        };
        p.padM.onmousedown = function (e) {
          // If the slider is at the bottom, move it up.
          switch (modeID) {
            case 0:
              if (THIS.hsv[2] === 0) {
                THIS.fromHSV(null, null, 1.0);
              }
              break;

            case 1:
              if (THIS.hsv[1] === 0) {
                THIS.fromHSV(null, 1.0, null);
              }
              break;

          }
          holdSld = false;
          holdPad = true;
          set_pad(e);
          dispatch_immediate_change();
        };
        if ('ontouchstart' in window) {
          p.padM.addEventListener('touchstart', function (e) {
            touchOffset = {
              X: e.target.offsetParent.offsetLeft,
              Y: e.target.offsetParent.offsetTop
            };
            this.onmousedown({
              offsetX: e.touches[0].pageX - touchOffset.X,
              offsetY: e.touches[0].pageY - touchOffset.Y
            });
          });
        }
        p.sldM.onmouseup =
        p.sldM.onmouseout = function () {
          if (holdSld) {
            holdSld = false; jscolor.fireEvent(valueElement, 'change');
          }
        };
        p.sldM.onmousedown = function (e) {
          holdPad = false;
          holdSld = true;
          set_sld(e);
          dispatch_immediate_change();
        };
        if ('ontouchstart' in window) {
          p.sldM.addEventListener('touchstart', function (e) {
            touchOffset = {
              X: e.target.offsetParent.offsetLeft,
              Y: e.target.offsetParent.offsetTop
            };
            this.onmousedown({
              offsetX: e.touches[0].pageX - touchOffset.X,
              offsetY: e.touches[0].pageY - touchOffset.Y
            });
          });
        }

        // Picker.
        var dims = get_picker_dims(THIS);
        p.box.style.width = dims[0] + 'px';
        p.box.style.height = dims[1] + 'px';

        // Picker border.
        p.boxB.style.position = 'absolute';
        p.boxB.style.clear = 'both';
        p.boxB.style.left = x + 'px';
        p.boxB.style.top = y + 'px';
        p.boxB.style.zIndex = THIS.pickerZIndex;
        p.boxB.style.border = THIS.pickerBorder + 'px solid';
        p.boxB.style.borderColor = THIS.pickerBorderColor;
        p.boxB.style.background = THIS.pickerFaceColor;

        // Pad image.
        p.pad.style.width = jscolor.images.pad[0] + 'px';
        p.pad.style.height = jscolor.images.pad[1] + 'px';

        // Pad border.
        p.padB.style.position = 'absolute';
        p.padB.style.left = THIS.pickerFace + 'px';
        p.padB.style.top = THIS.pickerFace + 'px';
        p.padB.style.border = THIS.pickerInset + 'px solid';
        p.padB.style.borderColor = THIS.pickerInsetColor;

        // Pad mouse area.
        p.padM.style.position = 'absolute';
        p.padM.style.left = '0';
        p.padM.style.top = '0';
        p.padM.style.width = THIS.pickerFace + 2 * THIS.pickerInset + jscolor.images.pad[0] + jscolor.images.arrow[0] + 'px';
        p.padM.style.height = p.box.style.height;
        p.padM.style.cursor = 'crosshair';

        // Slider image.
        p.sld.style.overflow = 'hidden';
        p.sld.style.width = jscolor.images.sld[0] + 'px';
        p.sld.style.height = jscolor.images.sld[1] + 'px';

        // Slider border.
        p.sldB.style.display = THIS.slider ? 'block' : 'none';
        p.sldB.style.position = 'absolute';
        p.sldB.style.right = THIS.pickerFace + 'px';
        p.sldB.style.top = THIS.pickerFace + 'px';
        p.sldB.style.border = THIS.pickerInset + 'px solid';
        p.sldB.style.borderColor = THIS.pickerInsetColor;

        // Slider mouse area.
        p.sldM.style.display = THIS.slider ? 'block' : 'none';
        p.sldM.style.position = 'absolute';
        p.sldM.style.right = '0';
        p.sldM.style.top = '0';
        p.sldM.style.width = jscolor.images.sld[0] + jscolor.images.arrow[0] + THIS.pickerFace + 2 * THIS.pickerInset + 'px';
        p.sldM.style.height = p.box.style.height;
        try {
          p.sldM.style.cursor = 'pointer';
        }
        catch (eOldIE) {
          p.sldM.style.cursor = 'hand';
        }

        // Close button.
        function set_btn_border() {
          var insetColors = THIS.pickerInsetColor.split(/\s+/);
          var pickerOutsetColor = insetColors.length < 2 ? insetColors[0] : insetColors[1] + ' ' + insetColors[0] + ' ' + insetColors[0] + ' ' + insetColors[1];
          p.btn.style.borderColor = pickerOutsetColor;
        }
        p.btn.style.display = THIS.pickerClosable ? 'block' : 'none';
        p.btn.style.position = 'absolute';
        p.btn.style.left = THIS.pickerFace + 'px';
        p.btn.style.bottom = THIS.pickerFace + 'px';
        p.btn.style.padding = '0 15px';
        p.btn.style.height = '18px';
        p.btn.style.border = THIS.pickerInset + 'px solid';
        set_btn_border();
        p.btn.style.color = THIS.pickerButtonColor;
        p.btn.style.font = '12px sans-serif';
        p.btn.style.textAlign = 'center';
        try {
          p.btn.style.cursor = 'pointer';
        }
        catch (eOldIE) {
          p.btn.style.cursor = 'hand';
        }
        p.btn.onmousedown = function () {
          THIS.hidePicker();
        };
        p.btnS.style.lineHeight = p.btn.style.height;

        // Load images in optimal order.
        var padImg;
        switch (modeID) {
          case 0:
            padImg = 'hs.png';
            break;

          case 1:
            padImg = 'hv.png';
            break;

        }
        p.padM.style.backgroundImage = "url('" + jscolor.getDir() + "cross.gif')";
        p.padM.style.backgroundRepeat = "no-repeat";
        p.sldM.style.backgroundImage = "url('" + jscolor.getDir() + "arrow.gif')";
        p.sldM.style.backgroundRepeat = "no-repeat";
        p.pad.style.backgroundImage = "url('" + jscolor.getDir() + padImg + "')";
        p.pad.style.backgroundRepeat = "no-repeat";
        p.pad.style.backgroundPosition = "0 0";

        // Place pointers.
        redraw_pad();
        redraw_sld();

        jscolor.picker.owner = THIS;
        document.getElementsByTagName('body')[0].appendChild(p.boxB);
      }

      function get_picker_dims(o) {
        var dims = [
          2 * o.pickerInset + 2 * o.pickerFace + jscolor.images.pad[0] +
          (o.slider ? 2 * o.pickerInset + 2 * jscolor.images.arrow[0] + jscolor.images.sld[0] : 0),
          o.pickerClosable ?
          4 * o.pickerInset + 3 * o.pickerFace + jscolor.images.pad[1] + o.pickerButtonHeight :
          2 * o.pickerInset + 2 * o.pickerFace + jscolor.images.pad[1]
        ];
        return dims;
      }
      var yComponent;
      function redraw_pad() {
        // Redraw the pad pointer.
        switch (modeID) {
          case 0: yComponent = 1; break;

          case 1: yComponent = 2; break;

        }
        var x = Math.round((THIS.hsv[0] / 6) * (jscolor.images.pad[0] - 1));
        var y = Math.round((1 - THIS.hsv[yComponent]) * (jscolor.images.pad[1] - 1));
        jscolor.picker.padM.style.backgroundPosition =
        (THIS.pickerFace + THIS.pickerInset + x - Math.floor(jscolor.images.cross[0] / 2)) + 'px ' +
        (THIS.pickerFace + THIS.pickerInset + y - Math.floor(jscolor.images.cross[1] / 2)) + 'px';

        // Redraw the slider image.
        var seg = jscolor.picker.sld.childNodes;

        switch (modeID) {
          case 0:
            var rgb = HSV_RGB(THIS.hsv[0], THIS.hsv[1], 1);
            var i;
            for (i = 0; i < seg.length; i += 1) {
              seg[i].style.backgroundColor = 'rgb(' +
              (rgb[0] * (1 - i / seg.length) * 100) + '%,' +
              (rgb[1] * (1 - i / seg.length) * 100) + '%,' +
              (rgb[2] * (1 - i / seg.length) * 100) + '%)';
            }
            break;

          case 1:
            var s; var c = [THIS.hsv[2], 0, 0];
            i = Math.floor(THIS.hsv[0]);
            var f = i % 2 ? THIS.hsv[0] - i : 1 - (THIS.hsv[0] - i);
            switch (i) {
              case 6:
              case 0: rgb = [0, 1, 2]; break;

              case 1: rgb = [1, 0, 2]; break;

              case 2: rgb = [2, 0, 1]; break;

              case 3: rgb = [2, 1, 0]; break;

              case 4: rgb = [1, 2, 0]; break;

              case 5: rgb = [0, 2, 1]; break;

            }

            for (i = 0; i < seg.length; i += 1) {
              s = 1 - 1 / (seg.length - 1) * i;
              c[1] = c[0] * (1 - s * f);
              c[2] = c[0] * (1 - s);
              seg[i].style.backgroundColor = 'rgb(' +
              (c[rgb[0]] * 100) + '%,' +
              (c[rgb[1]] * 100) + '%,' +
              (c[rgb[2]] * 100) + '%)';
            }
            break;
        }
      }

      function redraw_sld() {
        // Redraw the slider pointer.
        switch (modeID) {
          case 0: yComponent = 2; break;

          case 1: yComponent = 1; break;

        }
        var y = Math.round((1 - THIS.hsv[yComponent]) * (jscolor.images.sld[1] - 1));
        jscolor.picker.sldM.style.backgroundPosition =
        '0 ' + (THIS.pickerFace + THIS.pickerInset + y - Math.floor(jscolor.images.arrow[1] / 2)) + 'px';
      }

      function is_picker_owner() {
        return jscolor.picker && jscolor.picker.owner === THIS;
      }

      function blur_target() {
        if (valueElement === target) {
          THIS.importColor();
        }
        if (THIS.pickerOnfocus) {
          THIS.hidePicker();
        }
      }

      function blur_value() {
        if (valueElement !== target) {
          THIS.importColor();
        }
      }

      function set_pad(e) {
        var mpos = jscolor.getRelMousePos(e);
        var x = mpos.x - THIS.pickerFace - THIS.pickerInset;
        var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
        switch (modeID) {
          case 0: THIS.fromHSV(x * (6 / (jscolor.images.pad[0] - 1)), 1 - y / (jscolor.images.pad[1] - 1), null, leaveSld); break;

          case 1: THIS.fromHSV(x * (6 / (jscolor.images.pad[0] - 1)), null, 1 - y / (jscolor.images.pad[1] - 1), leaveSld); break;

        }
      }

      function set_sld(e) {
        var mpos = jscolor.getRelMousePos(e);
        var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
        switch (modeID) {
          case 0: THIS.fromHSV(null, null, 1 - y / (jscolor.images.sld[1] - 1), leavePad); break;

          case 1: THIS.fromHSV(null, 1 - y / (jscolor.images.sld[1] - 1), null, leavePad); break;

        }
      }

      function dispatch_immediate_change() {
        if (THIS.onImmediateChange) {
          var callback;
          if (typeof THIS.onImmediateChange === 'string') {
            callback = function () {
              return (THIS.onImmediateChange);
            };
          }
          else {
            callback = THIS.onImmediateChange;
          }
          callback.call(THIS);
        }
      }

      var THIS = this;
      var modeID = this.pickerMode.toLowerCase() === 'hvs' ? 1 : 0;
      var abortBlur = false;
      var valueElement = jscolor.fetchElement(this.valueElement);
      var styleElement = jscolor.fetchElement(this.styleElement);
      var holdPad = false; var holdSld = false; var touchOffset = {
        // Empty.
      };
      var leaveValue = 1; var leaveStyle = 2; var leavePad = 4; var leaveSld = 8;
      jscolor.isColorAttrSupported = false;
      var el = document.createElement('input');
      if (el.setAttribute) {
        el.setAttribute('type', 'color');
        if (el.type.toLowerCase() === 'color') {
          jscolor.isColorAttrSupported = true;
        }
      }

      // Target.
      jscolor.addEvent(target, 'focus', function () {
        if (THIS.pickerOnfocus) {
          THIS.showPicker();
        }
      });
      jscolor.addEvent(target, 'blur', function () {
        if (!abortBlur) {
          window.setTimeout(function () {
            if (!abortBlur) {
              blur_target();
            }
            abortBlur = false;
          }, 0);
        }
        else {
          abortBlur = false;
        }
      });

      // ValueElement.
      if (valueElement) {
        var updateField = function () {
          THIS.fromString(valueElement.value, leaveValue);
          dispatch_immediate_change();
        };
        jscolor.addEvent(valueElement, 'keyup', updateField);
        jscolor.addEvent(valueElement, 'input', updateField);
        jscolor.addEvent(valueElement, 'blur', blur_value);
        valueElement.setAttribute('autocomplete', 'off');
      }

      // StyleElement.
      if (styleElement) {
        styleElement.jscStyle = {
          backgroundImage: styleElement.style.backgroundImage,
          backgroundColor: styleElement.style.backgroundColor,
          color: styleElement.style.color
        };
      }

      // Require images.
      switch (modeID) {
        case 0: jscolor.requireImage('hs.png'); break;

        case 1: jscolor.requireImage('hv.png'); break;

      }
      jscolor.requireImage('cross.gif');
      jscolor.requireImage('arrow.gif');

      this.importColor();
    }

  };

  jscolor.install();
