<?php
/**
 * @file
 * Future_look theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 * The  Front Page Carousel Slider are not in this template.
 * Instead they can be found in
 * the slider.php file.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 *
 * $page['navigation']     = Items for the Navigation  region.
 * $page['header']         = Items for the header region.
 * $page['help']           = Dynamic help text, mostly for admin pages.
 * $page['content']        = The main content of the current page.
 * $page['introduction']   = Items for the Introduction Related Content region.
 * $page['right_bar']    = Items for the Rightbar.
 * $page['box_first']      = Items for the Box First.
 * $page['box_second']     = Items for the Box Secound.
 * $page['box_third']      = Items for the Box Third.
 * $page['content_bottom'] = Items for the Content Bottom.
 * $page['footer']         = Items for the footer region.
 * $page['page_top']       = Items for the Page top.
 * $page['page_bottom']    = Items for the Page bottom.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see theme_get_setting()
 * @see html.tpl.php
 * @see slider.php
 *
 * @ingroup themeable
 */

$facebook = theme_get_setting('facebook_link');
$twitter = theme_get_setting('twitter_link');
$linkedin = theme_get_setting('linkedin_link');
$gplus = theme_get_setting('gplus_link');
$f_breadcrumb  = theme_get_setting('future_look_breadcrumb');
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
   <div class="container">
     <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" 
             data-target="#navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      <?php if ($site_name): ?>
           <?php if ($title): ?>
             <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>                  
          <?php else: /* Use h1 when the content title is empty */ ?>
             <a class="navbar-brand"  href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          <?php endif; ?>
          <?php endif; ?>
     </div> <!-- navbar-header-->
     <div id="navbar" class="navbar-collapse collapse" >
         <div class="nav navbar-nav navbar-right">
            <?php print $primary_nav;  ?>
         </div><!-- /. navbar-right -->
         </div><!--/.nav-collapse -->
       </div><!-- /.container -->
    </nav> <!-- /.navbar -->
  <div id="header" class="section clearfix">
      <div class="container">
          <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6" id="header-left">
          <?php if ($logo): ?>
             <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" <?php if(!$site_slogan){?> class="slogan-fix" <?php } ?>/>
             </a>             
             <?php  endif; ?>
          <?php if ($site_slogan): ?>
                 <div id="site-slogan"<?php if (isset($hide_site_slogan)) {print ' class="element-invisible"';} ?>>
                     <?php print $site_slogan; ?>
                 </div><!-- /#site_slogan -->
                      <?php endif; ?>
          </div><!-- /col-sm-8 -->
         <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" id="right_social_bar">
            <?php if(isset($linkedin) && $linkedin != ""):?>
             <a class="btn btn-social-icon btn-linkedin" href="<?php print $linkedin; ?>">
             <i class="fa fa-linkedin"></i></a>
            <?php endif; ?>
          <?php if(isset($gplus) && $gplus != ""):?>
            <a class="btn btn-social-icon btn-google" href="<?php print $gplus; ?>">
            <i class="fa fa-google"></i></a>
          <?php endif; ?>
          <?php if(isset($twitter) && $twitter != ""):?>
            <a class="btn btn-social-icon btn-twitter" href="<?php print $twitter; ?>">
            <i class="fa fa-twitter"></i></a>
          <?php endif; ?>
         <?php if(isset($facebook) && $facebook != ""):?>
           <a class="btn btn-social-icon btn-facebook" href="<?php print $facebook; ?>">
           <i class="fa fa-facebook"></i></a>
         <?php endif; ?>
    </div>        
    <?php if($messages): ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php print $messages; ?>
    </div><!-- row -->
    <?php endif; ?>
    <?php if ($page['header']): ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php print render($page['header']); ?>
    </div>
    <?php endif; ?>     
</div><!-- container -->
</div><!-- header/section -->
  <div id="main" class="clearfix">
<?php
/**
 * Front Page Carousel Slider.
 */
          if ($is_front):?>
            <div class="container-fluid no-padding">
<?php
$slider_checkbox = theme_get_setting('default_slider');
if ($slider_checkbox == 1) {
  $text_area_slide1 = theme_get_setting('text_slide1');
  $text_area_slide2 = theme_get_setting('text_slide2');
  $text_area_slide3 = theme_get_setting('text_slide3');
  $text_area_slide4 = theme_get_setting('text_slide4');
  $text_area_slide5 = theme_get_setting('text_slide5');
  $text_area_slide6 = theme_get_setting('text_slide6');
  $slide_1_image = theme_get_setting('front_slide1');
  $slide_2_image = theme_get_setting('front_slide2');
  $slide_3_image = theme_get_setting('front_slide3');
  $slide_4_image = theme_get_setting('front_slide4');
  $slide_5_image = theme_get_setting('front_slide5');
  $slide_6_image = theme_get_setting('front_slide6');
  $flag = 0;
  if ($slide_1_image != 0) {
    $flag += 1;
  }
  if ($slide_2_image != 0) {
    $flag += 1;
  }
  if ($slide_3_image != 0) {
    $flag += 1;
  }
  if ($slide_4_image != 0) {
    $flag += 1;
  }
  if ($slide_5_image != 0) {
    $flag += 1;
  }
  if ($slide_6_image != 0) {
    $flag += 1;
  }
  ?>
  
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
  <ol class="carousel-indicators">
  <?php
  if ($flag >= 1) {
    ?>
   <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
  <?php
  }?>
  
  <?php
  if ($flag >= 2) {
    ?>
   <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  <?php
  }?>
  
  <?php
  if ($flag >= 3) {
    ?>
   <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <?php
  }?>
  
  <?php
  if ($flag >= 4) {
    ?>
   <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <?php
  }?>
  
  <?php
  if ($flag >= 5) {
    ?>
  <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    <?php
  }?>
  
  <?php
  if ($flag == 6) {
    ?>
  <li data-target="#carousel-example-generic" data-slide-to="5"></li>
    <?php
  }?>
  </ol>
  
  <div class="carousel-inner" role="listbox">
  <?php if ($slide_1_image != 0) {
    $slide_1_image_url = file_create_url(file_load($slide_1_image)->uri);
    ?>
    <div class="item active">
    
    <img src=<?php print $slide_1_image_url;?>  alt="First slide">
    <div class="carousel-caption">
    <p><?php
     print $text_area_slide1;
    ?>
    </p>
    </div> <!-- cro-caption-->
    </div> <!-- item active-->
  <?php
}?>
 <?php if ($slide_2_image != 0) {
    $slide_2_image_url = file_create_url(file_load($slide_2_image)->uri);?>
    <div class="item">
    <img src=<?php print $slide_2_image_url?> alt="Second slide">
    
    <div class="carousel-caption">
    <p><?php  print $text_area_slide2;?></p>
    </div> <!-- cro-caption-->
  </div> <!-- item -->
  <?php
}?>
  
  
  <?php if ($slide_3_image != 0) {
    $slide_3_image_url = file_create_url(file_load($slide_3_image)->uri);?>
    <div class="item">
    <img src=<?php print $slide_3_image_url?> alt="Third slide">
    
    <div class="carousel-caption">
    <p><?php  print $text_area_slide3;?></p>
    </div> <!-- cro-caption-->
  </div> <!-- item -->
  <?php
}?>
  
  <?php if ($slide_4_image != 0) {
    $slide_4_image_url = file_create_url(file_load($slide_4_image)->uri);?>
    <div class="item">
    <img src=<?php print $slide_4_image_url?> alt="fourth slide">
    
    <div class="carousel-caption"style='color:black;'>
    <p><?php  print $text_area_slide4;?></p>
    </div> <!-- cro-caption-->
    
  </div> <!-- item -->
  <?php
}?>
  
  <?php if ($slide_5_image != 0) {
    $slide_5_image_url = file_create_url(file_load($slide_5_image)->uri);?>
    <div class="item">
    <img src=<?php print $slide_5_image_url?> alt="Fifth slide">
    
    <div class="carousel-caption">
    <p><?php  print $text_area_slide5;?></p>
    </div> <!-- cro-caption-->
    
  </div> <!-- item-->
  <?php
}?>
  
  <?php if ($slide_6_image != 0) {
    $slide_6_image_url = file_create_url(file_load($slide_6_image)->uri);?>
    <div class="item">
    <img src=<?php print $slide_6_image_url?> alt="Sixth slide">
    
    <div class="carousel-caption">
    <p><?php  print $text_area_slide6;?></p>
    </div> <!-- cro-caption-->
    
  </div> <!-- item-->
  <?php
}?>
  </div> <!-- cro-inner-->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right"  aria-hidden="true"></span>
  <span class="sr-only">Next</span>
  </a>
</div> <!--carousel-example-generic -->
<?php
}?>
</div> <!--container-fluid no-padding -->

      <?php endif;
       ?>

   <?php if ($page['introduction']): ?>
   <div  id="introduction">
    <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php print render($page['introduction']); ?>  
    </div>
    </div><!-- /.container -->
   </div>  
   <?php endif; ?>
    
  <?php
  if($page['right_bar']){
    $content_classes = "col-xs-12 col-sm-12 col-md-9 col-lg-9";
  }else{
    $content_classes = "col-xs-12 col-sm-12 col-md-12 col-lg-12";
  }
  ?>
    
    <div id="content" >
      <div class="container">
     <?php if ($breadcrumb && $f_breadcrumb == 1): ?>
      <div id="breadcrumb" class='col-xs-12 col-sm-12 col-md-12 col-lg-12'><?php print $breadcrumb; ?></div>
    <?php endif; ?>
    <div class="<?php print $content_classes; ?>">
    <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul>
    <?php endif; ?>
    <?php print render($page['content']); ?>
    <?php print $feed_icons; ?>
    </div>
    <?php if($page['right_bar']): ?>
    <div id='rightbar' class=" col-xs-12 col-sm-6 col-md-3 col-lg-3 " style='padding-top:10px;' >
      <?php    print render($page['right_bar']); ?>  
    </div><!-- /.col-xs-12 col-sm-3 -->
    <?php endif; ?> 
  
     </div>   <!-- /.container -->
    </div> <!-- /#content -->

<div id='box' class="container marketing">

      <?php if ($page['box_first']): ?>
        <div id="box-first" class="col-sm-6 col-lg-4 col-xs-12">

          <?php print render($page['box_first']); ?> 
    </div><!-- col-lg-4 -->
  <?php endif; ?>

     <?php if ($page['box_second']): ?>
        <div id="box-second" class="col-sm-6 col-lg-4 col-xs-12" >
          <?php print render($page['box_second']); ?>

      </div><!--col-lg-4 -->
  <?php endif; ?>

     <?php if ($page['box_third']): ?>
    <div id="box-third" class="col-sm-6 col-lg-4 col-xs-12" >
          <?php print render($page['box_third']); ?>
     </div><!-- col-lg-4 -->
    <?php endif; ?>

     </div><!-- container -->    
    
    <?php if ($page['content_bottom']):   ?>
         <div  id="content_bottom">
    <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php print render($page['content_bottom']); ?>  
      </div>
    </div><!-- /.container -->
    </div>   
    <?php endif; ?>
     </div><!-- main -->
   
     <div id="gfont-fix">.</div>
 
     <!--<div class="panel-footer footer-background">-->
  
        <div class="footer-background">
      <div class='container'>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     <?php print render($page['footer']); ?>
     <div class="designed-by">Designed by <a href="http://www.drupal1000.com" target="_blank">Drupal1000.com</a></div>
        </div>
      </div><!-- /.container -->
      </div><!-- /.footer-background -->

 <!-- panal-footer-->
