<?php

/**
 * @file
 * Html template file.
 */
 ?>
<!DOCTYPE html>
<html lang="<?php print $language->language; ?>"  dir="<?php print $language->dir; ?>">
<head profile="<?php print $grddl_profile; ?>">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

  

  <?php print $styles; ?>
  <?php print $scripts; ?>
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <link href="<?php global $base_url; print $base_url . base_path() . path_to_theme();?>/css/ie8.css" rel="stylesheet" type="text/css">
  <link href="<?php  print $base_url . base_path() . path_to_theme();?>/css/font-awesome.css" rel="stylesheet" type="text/css">
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <script src="<?php  print $base_url . base_path() . path_to_theme();?>/js/ie8_custom.js"></script>
	  
  <![endif]-->

</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
